# Custom Nano RC

I use the [nano](https://www.nano-editor.org/) text editor frequently.
This repository contains some custom nanorc files I've written, e.g.
for syntax highlighting of languages not already supported by nano.

Also included is a script to regenerate the .nanorc file from a
template, with everything in this repository included.

They are free to use and redistribute under the terms of the MIT
License.

## Included Files

* verilog.nanorc: Verilog syntax highlighting, supports both Verilog
and SystemVerilog.

* vhdl.nanorc: VHDL syntax highlighting.
